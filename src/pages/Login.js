import React, { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    }
  }, [email, password]);

  const loginUser = (e) => {
    e.preventDefault();
    fetch('http://localhost:4000/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.token) {
          localStorage.setItem('token', data.token);
          Swal.fire({
            icon: 'success',
            title: 'Login Successful',
            text: `Welcome back!`,
          });
          fetch('http://localhost:4000/users/getUserDetails', {
            headers: {
              authorization: `Bearer ${data.token}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              console.log(user);
              setUser({
                id: data._id,
                isAdmin: data.isAdmin,
              });
            });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Login Failed.',
            text: data.message ? data.message : 'An error occured',
          });
        }
      })
      .catch((e) => {
        console.error(e);
        Swal.fire({
          icon: 'error',
          title: 'Uh Oh :(',
          text: e.message,
        });
      });
  };
  return user.id ? (
    <Redirect to="/courses" />
  ) : (
    <>
      <h1 className="my-5 text-center">Login</h1>
      <Form onSubmit={(e) => loginUser(e)}>
        <Form.Group>
          <Form.Label>Email:</Form.Label>
          <Form.Control
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Enter Email"
            required
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Enter Password"
            required
          />
        </Form.Group>
        <Button variant="primary" type="submit" disabled={!isActive}>
          Login
        </Button>
      </Form>
    </>
  );
}
