import React, { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Unauthorized from './Unauthorized';

export default function AddCourse() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [isActive, setIsActive] = useState(false);
  const { user } = useContext(UserContext);
  const history = useHistory();
  // const resetInputs = () => {
  //   setName('');
  //   setDescription('');
  //   setPrice('');
  // };
  useEffect(() => {
    if (name !== '' && description !== '' && price !== '') {
      setIsActive(true);
    }
  }, [name, description, price]);

  const createCourse = (e) => {
    e.preventDefault();
    fetch('http://localhost:4000/courses/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name,
        description,
        price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data._id) {
          Swal.fire({
            icon: 'success',
            title: 'Course Added',
            text: `The course ${name} was added successfully`,
          });
          history.push('/courses');
          // resetInputs();
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Failed',
            text: data.message ? data.message : 'An error occured',
          });
        }
      })
      .catch((e) => {
        console.error(e);
        Swal.fire({
          icon: 'error',
          title: 'Uh Oh :(',
          text: e.message,
        });
      });
  };

  return !user.isAdmin ? (
    <Unauthorized />
  ) : (
    <>
      <h1 className="my-5 text-center">Add Course</h1>
      <Form onSubmit={(e) => createCourse(e)}>
        <Form.Group>
          <Form.Label>Name:</Form.Label>
          <Form.Control
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Enter Course Name"
            required
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>Description:</Form.Label>
          <Form.Control
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Enter Description"
            required
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Price:</Form.Label>
          <Form.Control
            type="number"
            value={price}
            min={0}
            onChange={(e) => setPrice(e.target.value)}
            placeholder="Enter Price"
            required
          />
        </Form.Group>

        <Button variant="primary" type="submit" disabled={!isActive}>
          Add Course
        </Button>
      </Form>
    </>
  );
}
