import React, { useContext, useEffect, useState } from 'react';
import { Card, Button, Row, Col, Container } from 'react-bootstrap';
import { useParams, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ViewCourse() {
  const { courseId } = useParams();
  const { user } = useContext(UserContext);
  const history = useHistory();
  const [courseDetails, setCourseDetails] = useState({
    name: null,
    description: null,
    price: null,
  });

  useEffect(() => {
    fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data._id) {
          const { name, description, price } = data;
          setCourseDetails({
            name,
            description,
            price,
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Course Unavailable',
            text: data.message,
          });
        }
      });
  }, [courseId]);
  console.log(user);
  const enroll = (courseId) => {
    fetch('http://localhost:4000/users/enroll', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        courseId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
  };
  console.log(user);
  const { name, description, price } = courseDetails;
  return (
    <Container>
      <Row>
        <Col>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>&#8369; {price}</Card.Text>
              <Card.Subtitle>Schedule:</Card.Subtitle>
              <Card.Text>8am - 5pm</Card.Text>
              {user.isAdmin === false ? (
                <Button
                  variant="primary"
                  block
                  onClick={() => enroll(courseId)}
                >
                  Enroll
                </Button>
              ) : (
                <Link
                  variant="primary"
                  class="btn btn-danger btn-block"
                  to="/login"
                  block
                >
                  Login to Enroll
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
