import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(props) {
  const sampleProp = {
    title: 'Course Booking System',
    description: 'Get your course online',
    buttonCallToAction: 'Join Us',
    destination: '/login',
  };
  return (
    <>
      <Banner bannerProp={sampleProp} />
      <Highlights />
    </>
  );
}
