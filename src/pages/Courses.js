import React, { useState, useEffect, useContext } from 'react';
import coursesData from '../data/coursesData';
import Course from '../components/Course';
import UserContext from '../UserContext';
import { Table, Button } from 'react-bootstrap';

export default function Courses() {
  const [coursesCards, setCoursesCards] = useState([]);
  const [allCourses, setAllCourses] = useState([]);
  const [update, setUpdate] = useState(0);
  const { user } = useContext(UserContext);

  console.log(user);

  useEffect(() => {
    fetch('http://localhost:4000/courses/getActiveCourses')
      .then((res) => res.json())
      .then((data) => {
        setCoursesCards(
          data.map((course) => <Course key={course._id} courseProp={course} />)
        );
      })
      .catch((e) => {
        console.error(e);
      });
  }, []);

  const archive = (courseId) => {
    console.log('archive', courseId);
    fetch(`http://localhost:4000/courses/archive/${courseId}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUpdate({});
      })
      .catch((e) => {
        console.error(e);
      });
  };
  const activate = (courseId) => {
    fetch(`http://localhost:4000/courses/activate/${courseId}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUpdate({});
      })
      .catch((e) => {
        console.error(e);
      });
  };
  useEffect(() => {
    if (user.isAdmin) {
      fetch('http://localhost:4000/courses/', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setAllCourses(
            data.map((course) => {
              return (
                <tr key={course._id}>
                  <td>{course._id}</td>
                  <td>{course.name}</td>
                  <td>{course.price}</td>
                  <td>{course.isActive ? 'Active' : 'Inactive'}</td>
                  <td>
                    {course.isActive ? (
                      <Button
                        variant="danger"
                        className="mx-2"
                        onClick={() => archive(course._id)}
                      >
                        Archive
                      </Button>
                    ) : (
                      <Button
                        variant="success"
                        className="mx-2"
                        onClick={() => activate(course._id)}
                      >
                        Activate
                      </Button>
                    )}
                  </td>
                </tr>
              );
            })
          );
        })
        .catch((e) => {
          console.error(e);
        });
    }
  }, [user, update]);

  return user.isAdmin ? (
    <>
      <h1 className="text-center my-5">Course Dashboard</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Status</th>
            <th>ACtions</th>
          </tr>
        </thead>
        <tbody>{allCourses}</tbody>
      </Table>
    </>
  ) : (
    <>
      <h1 className="my-5">Available Courses</h1>
      {coursesCards}
    </>
  );
}
