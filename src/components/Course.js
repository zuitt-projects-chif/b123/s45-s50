import React, { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Course({ courseProp }) {
  const { name, description, price, _id: id } = courseProp;

  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(30);
  // const [isActive, setIsActive] = useState(true);

  //   let varCount = 0;
  // const enroll = () => {
  //   if (seats > 0) {
  //     setCount(count + 1);
  //     setSeats(seats - 1);
  //   }
  //   // varCount++;
  //   // console.log(varCount);
  // };
  // useEffect(() => {
  //   if (seats === 0) {
  //     setIsActive(false);
  //   }
  // }, [seats]);
  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>&#8369; {price}</Card.Text>
        <Link className="btn btn-primary" to={`/courses/${id}`}>
          View Course
        </Link>
      </Card.Body>
    </Card>
  );
}
