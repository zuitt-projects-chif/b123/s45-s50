import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
import { UserProvider } from './UserContext';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Home from './pages/Home';
import NotFound from './pages/NotFound';

import { BrowserRouter as Router } from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';
import coursesData from './data/coursesData';
import AddCourse from './pages/AddCourse';
import ViewCourse from './pages/ViewCourse';

export default function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });
  const unsetUser = () => {
    localStorage.clear();
  };
  useEffect(() => {
    fetch('http://localhost:4000/users/getUserDetails', {
      headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        console.log(user);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar userProp={user} />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/courses/add" component={AddCourse} />
            <Route exact path="/courses/:courseId" component={ViewCourse} />
            <Route exact path="/courses" component={Courses} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route component={NotFound} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  );
}
