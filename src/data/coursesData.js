const coursesData = [
  {
    id: 'wdc001',
    name: 'PHP-Laravel',
    description:
      'Nisi dolor duis amet nisi do eu quis reprehenderit tempor magna est sunt id.',
    price: 45000,
    onOffer: true,
  },
  {
    id: 'wdc002',
    name: 'Python-Django',
    description:
      'Anim dolore quis anim culpa consectetur et cupidatat aute do esse incididunt amet anim.',
    price: 50000,
    onOffer: true,
  },
  {
    id: 'wdc003',
    name: 'Java-Springboot',
    description: 'In non Lorem nisi do consectetur nostrud cillum sit ea.',
    price: 55000,
    onOffer: true,
  },
  {
    id: 'wdc004',
    name: 'Nodejs MERN',
    description:
      'ITempor sit adipisicing ea ex et reprehenderit elit proident.',
    price: 45000,
    onOffer: false,
  },
];

export default coursesData;
